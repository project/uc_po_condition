UC PO Condition
=========================
This module adds a new condition to Ubercart's Conditional Actions that checks
if an order is being delivered to a P.O. Box.  This allows store
administrators to restrict which shipping options are shown to customers who
are using a P.O. Box.  This is useful because some shipping carriers, such as
UPS, won't deliver to P.O. Boxes.

Installation Instructions
=========================
1) Download the module.
2) Untar it into sites/all/modules and enabble it in Drupal.
3) Add the condition to one of your Conditional Action predicates at
   Store Administration -> Conditional Actions

Known Limitations
=========================
Because customers type in their P.O. Box, it can be entered in any number of
ways.  The module trys to handle various punctuation and capitalization cases,
but some may still sneak through the filter.

Credits
=========================
Originally develoved by talbone and posted in this thread:
http://www.ubercart.org/contrib/6365
Updated the condition matching code based on code by cha0s' uc_nopobox
contribution.
